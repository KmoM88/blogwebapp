import { Component, OnInit, Input } from '@angular/core';

import { HttpService } from './../../services/http.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() titulo: string;

  usrName: string = '';
  usrId: string = '';
  usrAvatarUrl: string = '';

  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.usrId = this.httpService.getId();
    if(this.usrId)
      this.httpService.getUsrData(this.usrId).subscribe(
        res => {
          this.usrName = res.name,
          this.usrAvatarUrl = res.avatarUrl
        },
        err => {console.log(err)}
      )
  }

}
