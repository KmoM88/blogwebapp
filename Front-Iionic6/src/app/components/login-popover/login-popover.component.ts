import { Component, OnInit } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-login-popover',
  templateUrl: './login-popover.component.html',
  styleUrls: ['./login-popover.component.scss'],
})
export class LoginPopoverComponent implements OnInit {

  constructor(private navParams: NavParams , private popoverController: PopoverController) { }

  popoverMsg = null

  ngOnInit() {
    this.popoverMsg = this.navParams.get('popoverMsg');
  }

  onClick() {
    // console.log(this.popoverMsg)
    this.popoverController.dismiss();
  }

}
