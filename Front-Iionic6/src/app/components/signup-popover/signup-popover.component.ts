import { Component, OnInit } from '@angular/core';
import { PopoverController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-signup-popover',
  templateUrl: './signup-popover.component.html',
  styleUrls: ['./signup-popover.component.scss'],
})
export class SignupPopoverComponent implements OnInit {

  constructor(private navParams: NavParams , private popoverController: PopoverController) { }

  popoverMsg = null

  ngOnInit() {
    this.popoverMsg = this.navParams.get('popoverMsg');
  }

  onClick() {
    // console.log(this.popoverMsg)
    this.popoverController.dismiss();
  }
}
