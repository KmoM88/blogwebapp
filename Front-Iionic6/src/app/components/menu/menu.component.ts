import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Item } from './../../interfaces/Item';
import { ItemsService } from './../../services/items.service';
import { HttpService } from './../../services/http.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  menuItems: Observable<Item[]>;

  constructor(
    private itemsService: ItemsService,
    private httpService: HttpService
    ) { }

  ngOnInit() {
    this.menuItems = this.itemsService.getMenuItems();
  }

}
