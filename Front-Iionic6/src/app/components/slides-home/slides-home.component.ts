import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-slides-home',
  templateUrl: './slides-home.component.html',
  styleUrls: ['./slides-home.component.scss'],
})
export class SlidesHomeComponent implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  constructor() { }

  ngOnInit() {}

}
