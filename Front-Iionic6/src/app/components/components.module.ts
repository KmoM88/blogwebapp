import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuComponent } from './menu/menu.component';
import { HeaderComponent } from './header/header.component';
import { SlidesHomeComponent } from './slides-home/slides-home.component';
import { SignupPopoverComponent } from './signup-popover/signup-popover.component';
import { LoginPopoverComponent } from './login-popover/login-popover.component';

@NgModule({
  declarations: [
    HeaderComponent,
    MenuComponent,
    SlidesHomeComponent,
    SignupPopoverComponent,
    LoginPopoverComponent
  ],
  exports: [
    HeaderComponent,
    MenuComponent,
    SlidesHomeComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule
  ]
})
export class ComponentsModule { }
