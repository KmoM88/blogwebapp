import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UserLogin, UserSignup } from './../interfaces/User';
import { Post, PostServer } from './../interfaces/Post';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  serverUrl: string = 'http://localhost:3000/api';

  constructor(private http: HttpClient) { }

  signUp(user: UserSignup) {
    return this.http.post<any>(this.serverUrl + '/signup', user);
  }

  login(user: UserLogin) {
    return this.http.post<any>(this.serverUrl + '/login', user);
  }

  loggedIn() {
    return !!localStorage.getItem('jwt');
  }

  getPosts(){
    return this.http.get<PostServer[]>(this.serverUrl + '/post');
  }

  getPostById (postId: string) {
    return this.http.get<PostServer>(this.serverUrl + '/post/'+ postId);
  }

  createPost(post: Post){
    return this.http.post<any>(this.serverUrl + '/post',post);
  }

  deletePost(postId: string){
    return this.http.delete<any>(this.serverUrl + '/post/' + postId);
  }

  getToken() {
    return localStorage.getItem('jwt')
  }

  getId() {
    if (localStorage.getItem('jwt'))
      return localStorage.getItem('id')
  }

  getUsrData(id: any) {
    return this.http.get<any>(this.serverUrl + '/users/'+ id);
  }
}
