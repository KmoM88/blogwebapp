import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'posts',
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/posts/posts.module').then( m => m.PostsPageModule),
        canActivate: [AuthGuard]
      },
      {
        path: ':postId',
        loadChildren: () => import('./pages/post-detail/post-detail.module').then( m => m.PostDetailPageModule),
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path: 'post-create',
    loadChildren: () => import('./pages/post-create/post-create.module').then( m => m.PostCreatePageModule),
    canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
