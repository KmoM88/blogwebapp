import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { HttpService } from './../../services/http.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  isLogged: boolean = false;
  usrName: string = '';
  usrAvatarUrl: string = '';
  usrId: string = '';

  constructor(
    private httpService: HttpService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(() => {
      this.isLogged = this.httpService.loggedIn();
      this.usrId = this.httpService.getId();
      if(this.usrId)
        this.httpService.getUsrData(this.usrId).subscribe(
          res => {
            this.usrName = res.name,
            this.usrAvatarUrl = res.avatarUrl
          },
          err => {console.log(err)}
        )
    })
  }

}
