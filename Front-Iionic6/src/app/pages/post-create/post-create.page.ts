import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { Post } from './../../interfaces/Post';
import { HttpService } from './../../services/http.service';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.page.html',
  styleUrls: ['./post-create.page.scss'],
})
export class PostCreatePage implements OnInit {

  constructor(
    private httpService: HttpService,
    private router: Router
    ) { }

  post: Post = {
    usrId: '',
    name: '',
    title: '',
    body: ''
  }
  serverAnswer: string = '';

  @ViewChild('formulario') formulario: NgForm;

  ngOnInit() {
    this.post.usrId = this.httpService.getId();
    this.httpService.getUsrData(this.post.usrId).subscribe(
      res => {this.post.name  = res.name},
      err => {console.log(err)}
    )
  }

  onPostSubmit() {
    this.httpService.createPost(this.post).subscribe(
      res => {
        this.serverAnswer = JSON.stringify(res);
        // console.log(this.serverAnswer);
        this.router.navigate(['/posts']);
      },
      error => {
        this.serverAnswer = JSON.stringify(error);
        console.log(this.serverAnswer);
      }
    );
  }
}
