import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonList, ToastController, AlertController } from '@ionic/angular';

import { HttpService } from './../../services/http.service';
import { PostServer } from './../../interfaces/Post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.page.html',
  styleUrls: ['./posts.page.scss'],
})
export class PostsPage implements OnInit {

  @ViewChild('list') list: IonList;

<<<<<<< HEAD
  usrId = '';
=======
  usrId: string = '';
  loading: any;
>>>>>>> 7591ea9f97558190a4cc3c3fb5c4cb18683456ef

  constructor(
    private httpService: HttpService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastCtrl: ToastController,
    private alertController: AlertController
    ) { }
  posts: PostServer[] = [];

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(() => {
      this.httpService.getPosts().subscribe(
        res => {
          this.posts = res;
        },
        err => {
<<<<<<< HEAD
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this.router.navigate(['/login']);
            }
          }
=======
          if (err instanceof HttpErrorResponse)
            if (err.status === 401){
              this.router.navigate(['/login']);
            }
>>>>>>> 7591ea9f97558190a4cc3c3fb5c4cb18683456ef
        }
      );
    });
    this.usrId = this.httpService.getId();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  async presentAlert(postId: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Borrado de contenido',
      message: 'Está seguro que desea borrar este post?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.presentToast('Acción cancelada');
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.httpService.deletePost(postId).subscribe(
              res => {this.presentToast('Post borrado'); },
              err => {console.log(err); }
            )
          }
        }
      ]
    });

    await alert.present();
  }

  favoritePost(post) {
    console.log('Favorito');
    this.list.closeSlidingItems();
  }

  sharePost(post) {
    console.log('Compartir');
    this.list.closeSlidingItems();
  }

  deletePost(post){
    if (post.usrId === this.usrId){
      this.presentAlert(post._id)
    }
    else{
      this.presentToast('Solo puedes borrar tus posts');
    }
    this.list.closeSlidingItems();
  }
}
