import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { PopoverController } from '@ionic/angular';

import { UserSignup } from '../../interfaces/User';
import { SignupPopoverComponent } from '../../components/signup-popover/signup-popover.component';
import { HttpService } from './../../services/http.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  @ViewChild('formulario') formulario: NgForm;

  user: UserSignup = {
    name: '',
    email: '',
    psw: ''
  };
  serverAnswer = '';

  constructor(
    private popoverController: PopoverController,
    private httpService: HttpService,
    private router: Router

    ) { }

  ngOnInit() {
  }

  async callPop(event, popoverMsg?) {
    console.log(popoverMsg)
    const popover = await this.popoverController.create({
      component: SignupPopoverComponent,
      event,
      componentProps: {popoverMsg},
      mode: 'ios'
    });
    await popover.present();

    await popover.onDidDismiss();
  }

  onUserSubmit(event) {
    if(!this.formulario.valid) {
      this.callPop(event);
    }
    else {
      this.httpService.signUp(this.user).subscribe(
        res => {
          this.serverAnswer = JSON.stringify(res)
          // console.log(this.serverAnswer)
          localStorage.setItem('id', res.id)
          localStorage.setItem('jwt', res.jwt)
          this.router.navigate(['/home']);
        },
        error => {
          this.serverAnswer = JSON.stringify(error)
          if(this.serverAnswer.includes('That email is already registred')) {
            this.callPop(event, 'That email is already registred');
          }
          else if(this.serverAnswer.includes('"status":0')) {
            this.callPop(event, 'Check your connection');
          }
        }
      );
    }
  }
}
