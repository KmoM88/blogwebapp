import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonList, ToastController, AlertController } from '@ionic/angular';


import { PostServer } from './../../interfaces/Post';
import { HttpService } from './../../services/http.service';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.page.html',
  styleUrls: ['./post-detail.page.scss'],
})
export class PostDetailPage implements OnInit {

  loadedPost: PostServer;
  usrId: string = '';

  constructor(
    private httpService: HttpService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastCtrl: ToastController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      if(!paramMap.has('postId')){
        return
      }
      const postId = paramMap.get('postId');
      this.httpService.getPostById(postId).subscribe(
        res => {
          this.loadedPost = res
          // console.log(this.loadedPost)
        },
        err => {console.log(err)}
      );
    })
    this.usrId = this.httpService.getId();
  }

  async presentToast(message: string) {
    const toast = await this.toastCtrl.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  async presentAlert(postId: string) {
    console.log(postId)
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert',
      subHeader: 'Borrado de contenido',
      message: 'Está seguro que desea borrar este post?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.presentToast('Acción cancelada');
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.httpService.deletePost(postId).subscribe(
              res => {
                this.presentToast('Post borrado');
                this.router.navigate(['posts']);
              },
              err => {console.log(err)}
            )
          }
        }
      ]
    });
    await alert.present();
  }

  editPost() {}

  deletePost() {
    this.presentAlert(this.loadedPost._id)
  }

}
