import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { PopoverController } from '@ionic/angular';

import { UserLogin } from '../../interfaces/User';
import { LoginPopoverComponent } from './../../components/login-popover/login-popover.component';
import { HttpService } from './../../services/http.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  @ViewChild('formulario') formulario: NgForm;

  user: UserLogin = {
    lastLogin: null,
    email: '',
    psw: '',
  };
  serverAnswer: string = '';

  constructor(
    private popoverController: PopoverController,
    private httpService: HttpService,
    private router: Router

    ) { }

  ngOnInit() {
  }

  async callPop(event, popoverMsg?) {
    console.log(popoverMsg)
    const popover = await this.popoverController.create({
      component: LoginPopoverComponent,
      event: event,
      componentProps: {popoverMsg: popoverMsg},
      mode: 'ios'
    });
    await popover.present();

    await popover.onDidDismiss();
  }

  onLoginSubmit(event) {
    this.user.lastLogin = Date.now();
    if(!this.formulario.valid)
      this.callPop(event);
    else {
      this.httpService.login(this.user).subscribe(
        res => {
          this.serverAnswer = JSON.stringify(res)
          localStorage.setItem('id', res.id)
          localStorage.setItem('jwt', res.jwt)
          this.router.navigate(['/home']);
        },
        error => {
          this.serverAnswer = JSON.stringify(error)
          if(this.serverAnswer.includes('That mail is not registred'))
            this.callPop(event, 'That mail is not registred');
          else if(this.serverAnswer.includes('That password is incorrect'))
            this.callPop(event, "That password is incorrect");
          else if(this.serverAnswer.includes('"status":0'))
            this.callPop(event, "Check your connection");
        }
      );
    }
  }
}
