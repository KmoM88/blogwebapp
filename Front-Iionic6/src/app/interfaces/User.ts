export interface UserSignup {
  name: string,
  email: string,
  psw: string
}

export interface UserLogin {
  lastLogin: number,
  email: string,
  psw: string
}

