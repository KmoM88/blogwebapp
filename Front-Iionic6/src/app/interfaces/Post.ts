export interface Post {
  usrId: string
  name: string,
  title: string,
  body: string
}

export interface PostServer {
  _id: string,
  usrId: string,
  name: string,
  title: string,
  body: string
}
