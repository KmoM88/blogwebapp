const express = require('express');
const userController = require('../controllers/userController');
const postController = require('../controllers/postController');
const requireAuth = require('../middleware/userMiddleware');

const router = express.Router();

// Rutas de la API
// Users
router.get('/users', requireAuth, userController.users_get);
router.get('/users/:usrId', requireAuth, userController.userNameById_get)
router.post('/signup', userController.signup_post);
router.post('/login', userController.login_post);
router.get('/logout', requireAuth, userController.logout_get);

// post
router.get('/post', requireAuth, postController.post_get);
router.get('/post/search/:byName', requireAuth, postController.post_get_search_byName);
router.get('/post/:postId', requireAuth, postController.post_get_id);
router.post('/post', requireAuth, postController.post_post);
router.put('/post/:postId', requireAuth, postController.post_put_id);
router.delete('/post/:postId', requireAuth, postController.post_delete_id);

module.exports = router;