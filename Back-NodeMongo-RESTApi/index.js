//Paquetes usados
const express = require('express');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const cors = require('cors');
require('dotenv').config();
//Archivos propios
const routes = require('./routes/routes');
const { requireAuth, checkUser } = require('./middleware/userMiddleware');

const app = express();

//middleware
app.use(express.json());
app.use(cookieParser());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// view engine
app.set('view engine', 'ejs');

// database connection
const dbURI = process.env.DIR_DB_USR;
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true })
  .then((result) => app.listen(3000), console.log('REST API server iniciado'))
  .catch((err) => console.log(err));
mongoose.set('useFindAndModify', false);

//Routes Home
app.get('/', (req, res) => res.render('home'));
app.use('/api', routes);