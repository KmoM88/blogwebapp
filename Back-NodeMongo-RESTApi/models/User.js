const mongoose = require('mongoose');
const {isEmail} = require('validator');
const bcrypt = require('bcrypt');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please enter your name']
    },
    email: {
        type: String,
        required: [true, 'Please enter an email'],
        unique: true,
        lowercase: true,
        validate: [isEmail, 'Please enter a valid email']
    },
    psw: {
        type: String,
        required: [true, 'Please enter a password'],
        minlength: [6, 'Minimum password length is 6 characters']
    },
    signUpDate: {
        type: Date,
        default: Date.now(),
        required: false
    },
    lastLogin: {
        type: Date,
        required: false
    },
    avatarUrl: {
        type: String,
        required: false
    }
});

//fire function before userData saved to db
userSchema.pre('save', async function (next) {
    // console.log('nuevo usuario está por ser creado', this);
    const salt = await bcrypt.genSalt(10);
    //console.log('SALT GENERADO', salt, this.psw);
    try {
        this.psw = await bcrypt.hash(this.psw, salt);
        //console.log('SALT GENERADO', salt, this.psw);
    }
    catch (err) {
        console.log(this.psw, salt, err);
    }
    next();
})

//fire function after userData is saved to db
userSchema.post('save', function (userData, next){
    console.log('nuevo usuario creado');
    next();
});

//Static method to login user
userSchema.statics.login = async function(email, psw) {
    const user = await this.findOne({email});
    // console.log(user);
    if(user){
        const auth = await bcrypt.compare(psw, user.psw);
        if(auth) {
            // console.log(email, psw);
            return user;
        }
        throw Error('incorrect password');
    }
    throw Error('incorrect email');
}

const User = mongoose.model('user', userSchema);

module.exports = User;