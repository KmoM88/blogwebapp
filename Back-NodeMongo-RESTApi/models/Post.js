const mongoose = require('mongoose');

const postSchema = new mongoose.Schema({
    usrId: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: false
    },
    imgUrl: {
        type: String,
        required: false
    }
})

const Post = mongoose.model('Post', postSchema);
module.exports = Post;