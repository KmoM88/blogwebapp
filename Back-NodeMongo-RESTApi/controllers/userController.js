const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const crypto = require('crypto')
const User = require('../models/User');

//handle errors
const handleErrors = (err) => {
    console.error(err.message, err.code);
    let errors = { name: "",email: "", psw: "" };

    //incorrect email error
    if(err.message === "incorrect email"){
      errors.email = 'That mail is not registred';
    }

    //incorrect psw error
    if(err.message === "incorrect password"){
      errors.psw = 'That password is incorrect';
    }

    //duplicate mail error
    if (err.code === 11000) {
      errors.email = "That email is already registred";
    }

    //invalid name error
    if(err.message.includes("name: Please enter your name")){
      errors.name = 'Please enter your name';
      }

    //invalid email error
    if(err.message.includes("email: Please enter an email")){
      errors.email = 'Please enter an email';
      }
    if(err.message.includes("email: Please enter a valid email")){
      errors.email = 'Please enter a valid email';
      }

    //incvalid psw error
    if(err.message.includes("psw: Please enter a password")){
      errors.psw = 'Please enter a password';
      }
    if(err.message.includes("psw: Minimum password length is 6 characters")){
      errors.psw = 'Minimum password length is 6 characters';
      }

    //validation errors
    if (err.message.includes("User validation failed")) {
      Object.values(err.errors).forEach(({ properties }) => {
        errors[properties.path] = properties.message;
      });
    }
    return errors;
  };

// Funcion para generar JWT
const maxAge = 14*24*60*60;//Duración del token 14 días
const createToken = (id) => {
    return jwt.sign({id} , process.env.SECRET, {
        expiresIn: maxAge,
    });
};

// Metodos de petición
module.exports.users_get = async (req, res) => {
  User.find().sort({createdAt: -1})
      .then(result => {
          res.json(result);
      })
}

module.exports.userNameById_get = async (req, res) => {
  const usrId = req.params.usrId;
  User.findById(usrId)
  .then(result => {
      // console.log(result)
      res.status(200).json(result);
  })
  .catch(err =>{
      const errors = handleErrors(err);
      res.status(400).json(errors);
  })
}

module.exports.signup_post = async (req, res) => {
    const {name, email, psw} = req.body;
    // console.log(req.body);
    try{
        let hash = crypto.createHash('md5').update(email).digest("hex")
        // console.log(hash)
        let genAvatarUrl = `https://2.gravatar.com/avatar/${hash}s=32&d=retro&r=PG`;
        // console.log(genAvatarUrl)
        //avatarUrl = `https://2.gravatar.com/avatar/${}`;
        const user = await User.create({name, email, psw, lastLogin: Date.now(), avatarUrl: genAvatarUrl});
        const token = createToken(user._id);
        res.cookie('jwt', token, {maxAge: maxAge*1000});
        res.status(200).json({
          jwt: token,
          id: user._id
        });
    } catch (err){
        // console.log(err);
        const errors = handleErrors(err);
        res.status(400).json(errors);
    }
}

module.exports.login_post = async (req,res) => {
    const { email, psw, lastLogin } = req.body;
    try {
        const user = await User.login(email, psw);
        const token = createToken(user._id);
        const options = {
          new: true,//devolver el modificado
          upsert: false,//crearlo si no existe
          runValidators: true,//run validators
        }
        User.findByIdAndUpdate(user.id, {lastLogin}, options)
          .then(result => {
              // console.log(result)
              // res.status(200).json(result);
          })
          .catch(err =>{
              // console.log(err)
              const errors = handleErrors(err);
              res.status(400).json(errors);
          })
        res.cookie('jwt', token, {httpOnly: true, maxAge: maxAge*1000});
        res.status(200).json({
          jwt: token,
          id: user._id
        })
    } catch (err) {
        const errors = handleErrors(err);
        res.status(401).json(errors);
    }
  };

module.exports.logout_get = async (req, res) => {
    res.cookie('jwt', '', {maxAge: 1});
    res.redirect('/');
}