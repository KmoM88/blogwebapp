const Post = require('../models/Post.js');

// Handle Errors
const handleErrors = (err) => {
    // console.error(err.message, err.code);
    let errors = {message: '', code: ''};
    errors.message = err.message;
    errors.code = err.code;
    return errors;
}


// Métodos de Petición
module.exports.post_get = async (req, res) => {
    Post.find().sort({ createdAt: -1 })
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err => {
            const errors = handleErrors(err);
            res.status(400).json(errors);
        })
}

module.exports.post_get_id = async (req, res) => {
    const postId = req.params.postId;
    Post.findById(postId)
        .then(result => {
            console.log(result)
            res.status(200).json(result);
        })
        .catch(err =>{
            const errors = handleErrors(err);
            res.status(400).json(errors);
        })
}

module.exports.post_get_search_byName = async (req, res) => {
    const searchByName = req.params.byName;
    Post.find({ name: searchByName }).sort({ createdAt: -1 })
        .then(result => {
            res.status(200).json(result);
        })
        .catch(err => {
            const errors = handleErrors(err);
            res.status(400).json(errors);
        })
}

module.exports.post_post = async (req, res) => {
    const post = new Post(req.body);
    post.save()
        .then(result => res.status(200).json(result))
        .catch(err => {
            const errors = handleErrors(err);
            res.status(400).json(errors);
        })
}

module.exports.post_put_id = async (req, res) => {
    const postId = req.params.postId;
    const postUpdate = req.body;
    const options = {
        new: true,//devolver el modificado
        upsert: false,//crearlo si no existe
        runValidators: true,//run validators
    }
    Post.findByIdAndUpdate(postId, postUpdate, options)
        .then(result => {
            console.log(result)
            res.status(200).json(result);
        })
        .catch(err =>{
            const errors = handleErrors(err);
            res.status(400).json(errors);
        })
}

module.exports.post_delete_id = async (req, res) => {
    const postId = req.params.postId;
    Post.findByIdAndDelete(postId)
        .then(result => {
            console.log(result)
            res.status(200).json(result);
        })
        .catch(err =>{
            const errors = handleErrors(err);
            res.status(400).json(errors);
        })
}