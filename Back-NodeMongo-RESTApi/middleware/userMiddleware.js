const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const User = require('../models/User');

const requireAuth = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1];
    // console.log("TEST", token);
    if(token){
        jwt.verify(token, process.env.SECRET, (err, decodedToken) => {
            if(err){
                console.log(err.message);
                res.status(401).json({message: "Debe identificarse para acceder a este endpoint"});
            }
            else {
                // console.log(decodedToken);
                next();
            }
        });
    }
    else{
        res.status(401).json({message: "Debe identificarse para acceder a este endpoint"});
    }
}

module.exports = requireAuth;